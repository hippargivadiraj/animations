//
//  ContentView.swift
//  Animations
//
//  Created by Leadconsultant on 11/19/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var scaleChanger:CGFloat = 1.0
    
    var body: some View {
        Button("Tap Me"){
            self.scaleChanger += 1
            }.frame(width: 100 , height: 100 , alignment: .center).background(Color.white).foregroundColor(.red).clipped()
            .overlay(
            Circle()
             .stroke(Color.red)
         .scaleEffect(scaleChanger)
//       .opacity(Double(2 - scaleChanger))
             .animation(Animation.easeOut(duration: 1)
                  .repeatCount(5)
           )
    )
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
